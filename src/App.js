import React from "react";
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import "./App.css";

import Dashboard from "./components/Dashboard.jsx";

class App extends React.Component {
  render(){ 
    return (
      <BrowserRouter>
        <Switch>
          <Route path="/" render={props => <Dashboard {...props} />} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
