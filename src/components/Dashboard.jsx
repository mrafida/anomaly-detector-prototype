import React from "react";
import Dropzone from 'react-dropzone-uploader';
import 'react-dropzone-uploader/dist/styles.css';

export default class ContentWrapper extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			status: "init",
			isUploading: false,
			videoHash: ""
		};

		this.downloadFile = this.downloadFile.bind(this);
	}

	showDropzone = () => {
		if(this.state.isUploading || this.state.status !== "init"){
			this.setState({
				status: "init",
				isUploading: false
			})
		}
	}

	// specify upload params and url for your files
	getUploadParams = ({ meta }) => {
		return { url: 'http://10.181.131.252/predict' }
	}

	// called every time a file's `status` changes
	handleChangeStatus = ({meta, file, xhr}, status) => {
		// console.log(status, meta, xhr)

		if(xhr && xhr.response){
			this.setState({
				videoHash: JSON.parse(xhr.response).message
			})
		}
		if(status === 'ready'){
			meta.status = 'headers_received';
			this.setState({
				status: "headers_received"	
			})
		}
		if(status === "done"){
			this.setState({
				status: "done"
			})
		}
		if(status === "uploading"){
			this.setState({
				status: "uploading"
			})
		}
	}

	// receives array of files that are done uploading when submit button is clicked
	handleSubmit = (files, allFiles) => {
		// console.log(files.map(f => f.meta))
		allFiles.forEach(f => f.restart())
		this.setState({
			isUploading: true
		})
	}

	downloadFile = (videoHash) => {
		this.setState({
			status: "downloading"
		});

		fetch('http://10.181.131.252/download'
		,{
			method: 'POST',
			headers: {
			  'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				"uid": videoHash
			})
		})
		.then(res => res.blob())
		.then(blob => {
            var url = window.URL.createObjectURL(blob);
            var anchor = document.createElement('a');
            anchor.href = url;
            anchor.download = `${(new Date()).toLocaleString()}.mp4`;
            document.body.appendChild(anchor);
            anchor.click();    
            anchor.remove();

			this.setState({
				status: "done"
			});
		})
		.catch(error => {
			console.error('Error:', error);
		});
	}

	render() {
		return (
			<div style={{ height: "100vh", display: 'flex', flexDirection: "column", justifyContent: "center" }}>
				<div style={{ position: "absolute", top: "0", width: "100%", backgroundColor: "#008aa6", color: "#FFFFFF" }}>
					<div style={{ padding: "20px" }}>Anomaly Detection Web App</div>
				</div>
				<Dropzone
					maxFiles={1}
					autoUpload={false}
					getUploadParams={this.getUploadParams}
					onChangeStatus={this.handleChangeStatus}
					onSubmit={this.handleSubmit}
					submitButtonDisabled={() => (this.state.status !== "headers_received" && this.state.status !== "done")}
					accept="video/*"
					inputContent={
						props => <DropzoneDefault {...props} showDropzone={this.showDropzone}/>
					}
					SubmitButtonComponent={
						props => <TheButtons {...props} {...this.state} downloadFile={this.downloadFile}/>
					}
					LayoutComponent={
						props => <TheLayout {...props} {...this.state}/>
					}
				/>
			</div>
		)
	}
}

const DropzoneDefault = ({showDropzone}) => {
	showDropzone();

	return (
		<div style={{ textAlign: "center", padding: "50px 100px", border: "2px dotted #e0e0e0", backgroundColor: "#f7f7f7" }}>
			<img src="/images/video-add.svg" width="50px" alt="logo"/>
			<div style={{ color: "#bfbfbf", fontWeight: "600", fontSize: "15px", marginTop: "10px" }}>Drag and drop or browse your video</div>
		</div>
	)
}

const TheLayout = ({ isUploading, status, input, previews, submitButton, dropzoneProps, files, extra: { maxFiles } }) => {

	var theComponent = previews[0];
	var fileSize = 0;
	var uploadedSize = 0;
	var progress = 0;

	if(theComponent){
		// theComponent.props.meta.name = "SUP";
		var metaSize = theComponent.props.meta.size;
		fileSize = Math.round((metaSize / Math.pow(10,6) + Number.EPSILON) * 10) / 10;
		progress = Math.round((theComponent.props.meta.percent + Number.EPSILON) * 100) / 100;
		uploadedSize = Math.round((metaSize * progress / 100 / Math.pow(10,6) + Number.EPSILON) * 10) / 10;
	}

	return (
			<div {...dropzoneProps}>
				{files.length > 0 &&
					<div style={{ width: "525px", marginBottom: "35px" }}>
						<div style={{ display: "flex", width: "100%" }}>
							<div style={{ marginRight: "15px" }}><img src="/images/video-export.svg" width="50px" alt="logo"/></div>
							<div style={{ width: "100%" }}>
								<div style={{ width: "100%" }}>
									{theComponent? theComponent : null}
								</div>
									<div style={{ width: "95%", display: "flex", justifyContent: "space-between",
										marginTop: "5px", fontSize: "14px", color: "#a3a3a3" }}>
										{ !isUploading && status === "headers_received" && "Siap untuk diupload" }
										{ isUploading &&
											<>
												<div>
													{uploadedSize} MB of {fileSize} MB
												</div>
												<div style={{ color: "#069cc9" }}>
													{(status === "done")? "Done" : (status === "downloading")? "Downloading..." : "Uploading..."} {progress} %
												</div>
											</>}
									</div>
							</div>
						</div>
					</div>
				}
				{files.length < maxFiles && input}
				{files.length > 0 && submitButton}
			</div>
	)
}

const TheButtons = ({onSubmit, files, status, videoHash, downloadFile}) => {
	const handleUpload = () => {
		if(status === "headers_received" || status === "done"){
			onSubmit(files.filter(f => ['headers_received', 'done'].includes(f.meta.status)))
		}
	}

	return (
		<div style={{ display: "flex" }}>
		    {(status === "headers_received" || status === "done")?
		    	<label
			    	style={{ backgroundColor: '#008aa6', color: '#fff', cursor: 'pointer',
			    		padding: "12.5px 20px", margin: "0px 5px", borderRadius: 35 }}
			    	onClick={handleUpload}
			    >
			    	Detect Anomaly
			    </label>
			    :
			    <label
			    	style={{ backgroundColor: '#d4d4d4', color: '#858585', cursor: 'pointer',
			    		padding: "12.5px 20px", margin: "0px 5px", borderRadius: 35 }}
			    	onClick={e => e.preventDefault()}
			    >
			    	Detect Anomaly
			    </label>
			}
		    {(status === "done")?
		    	<label
			    	style={{ backgroundColor: '#66a5e3', fontSize: "15px", color: '#fff', cursor: 'pointer',
			    		padding: "12.5px 20px", margin: "0px 5px", borderRadius: 35 }}
			    	onClick={e => downloadFile(videoHash)}
			    >
			    	Download
			    </label>
				:
				<label
			    	style={{ backgroundColor: '#d4d4d4', fontSize: "15px", color: '#858585', cursor: 'pointer',
			    		padding: "12.5px 20px", margin: "0px 5px", borderRadius: 35, display: "flex" }}
			    	onClick={e => e.preventDefault()}
			    >
			    	Download
			    	{(status === "downloading") &&
						<div style={{ marginLeft: "3px" }}>		    	
							<svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" style={{"margin":"auto","background":"rgba(255, 255, 255, 0)","display":"block","shapeRendering":"auto"}} width="20px" height="20px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
								<circle cx={50} cy={50} fill="none" stroke="#009da2" strokeWidth={10} r={35} strokeDasharray="164.93361431346415 56.97787143782138" transform="rotate(215.497 50 50)">
									<animateTransform attributeName="transform" type="rotate" repeatCount="indefinite" dur="0.5s" values="0 50 50;360 50 50" keyTimes="0;1" />
								</circle>
							</svg>
						</div>
			    	}
			    </label>
			}
		</div>
	)
}
